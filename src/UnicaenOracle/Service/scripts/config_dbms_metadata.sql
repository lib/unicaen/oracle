--
-- DBMS_METADATA config
--

begin
  DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM, 'PRETTY',        true); -- formatting the output with indentation and line feeds ?
  DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM, 'SQLTERMINATOR', true); -- appending a SQL terminator (; or /) to each DDL statement ?
  --
  DBMS_METADATA.set_transform_param(DBMS_METADATA.SESSION_TRANSFORM, 'CONSTRAINTS_AS_ALTER', false);
  DBMS_METADATA.set_transform_param(DBMS_METADATA.SESSION_TRANSFORM, 'CONSTRAINTS',          true);  -- including non-referential table constraints in the CREATE TABLE statement ?
  DBMS_METADATA.set_transform_param(DBMS_METADATA.SESSION_TRANSFORM, 'REF_CONSTRAINTS',      false); -- including referential constraints (foreign keys) in the CREATE TABLE statement ?
  DBMS_METADATA.set_transform_param(DBMS_METADATA.session_transform, 'FORCE',                true);  -- using the FORCE keyword in the CREATE VIEW statement ?
  DBMS_METADATA.set_transform_param(DBMS_METADATA.session_transform, 'TABLESPACE',           false); -- including tablespace clauses in the DDL ?
  DBMS_METADATA.set_transform_param(DBMS_METADATA.session_transform, 'SEGMENT_ATTRIBUTES',   false); -- including segment attributes clauses (physical attributes, storage attributes, tablespace, logging) in the DDL ?
  DBMS_METADATA.set_transform_param(DBMS_METADATA.session_transform, 'STORAGE',              false); -- including storage clauses in the DDL ?
end
;
