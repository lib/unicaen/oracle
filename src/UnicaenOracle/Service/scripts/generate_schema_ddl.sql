--
-- sequences
--

SELECT DBMS_METADATA.GET_DDL ('SEQUENCE', sequence_name, sequence_owner) as sql
FROM all_sequences
where sequence_owner = 'SYGAL'

UNION ALL


--
-- tables
--

SELECT DBMS_METADATA.get_ddl ('TABLE', table_name, owner) as sql
FROM   all_tables
WHERE  owner = 'SYGAL' and
  table_name not in (select mview_name from all_mviews) -- exclusion des tables liées à des vues matérialisées

UNION ALL


--
-- constraints : not null, unique
--

SELECT DBMS_METADATA.get_ddl ('CONSTRAINT', constraint_name, owner) as sql
from user_constraints
WHERE constraint_type in ('U') and
      index_name is null and -- écarte les contraintes liées automatiquement à un index car créées avec la table
      table_name not in (select mview_name from all_mviews) -- exclusion des tables liées à des vues matérialisées
      and owner = 'SYGAL'

-- select * from user_constraints WHERE  owner = 'SYGAL';

UNION ALL


--
-- indexes
--

SELECT DBMS_METADATA.get_ddl ('INDEX', index_name, owner) as sql
FROM   all_indexes i
WHERE  not exists (select * from user_constraints where index_name = i.index_name) -- écarte les index créés automatiquement car liés à une contrainte
  and owner = 'SYGAL'

--   select * from all_indexes WHERE  owner = 'SYGAL';

UNION ALL


--
-- packages
--

SELECT DBMS_METADATA.get_ddl ('PACKAGE_SPEC', OBJECT_NAME, owner) as sql
FROM   SYS.ALL_OBJECTS
WHERE  owner = 'SYGAL'
       and UPPER(OBJECT_TYPE) = 'PACKAGE'

UNION ALL


--
-- procedures and functions
--

SELECT DBMS_METADATA.get_ddl ('PROCEDURE', OBJECT_NAME, owner) as sql
FROM   SYS.ALL_OBJECTS
WHERE  owner = 'SYGAL'
       and UPPER(OBJECT_TYPE) = 'PROCEDURE'
UNION ALL
SELECT DBMS_METADATA.get_ddl ('FUNCTION', OBJECT_NAME, owner) as sql
FROM   SYS.ALL_OBJECTS
WHERE  owner = 'SYGAL'
       and UPPER(OBJECT_TYPE) = 'FUNCTION'

UNION ALL


--
-- packages
--

SELECT DBMS_METADATA.get_ddl ('PACKAGE_BODY', OBJECT_NAME, owner) as sql
FROM   SYS.ALL_OBJECTS
WHERE  owner = 'SYGAL'
       and UPPER(OBJECT_TYPE) = 'PACKAGE BODY'

UNION ALL


--
-- triggers
--

SELECT DBMS_METADATA.get_ddl ('TRIGGER', trigger_NAME, owner) as sql
FROM   ALL_TRIGGERS
WHERE  owner = 'SYGAL'

UNION ALL


--
-- views
--

SELECT DBMS_METADATA.get_ddl ('VIEW', view_name, owner) as sql
FROM   all_views
WHERE  owner      = 'SYGAL'

UNION ALL


--
-- materialized views
--

SELECT DBMS_METADATA.get_ddl ('MATERIALIZED_VIEW', mview_name, owner) as sql
FROM   all_mviews
WHERE  owner      = 'SYGAL'

UNION ALL


--
-- data inserts
--

--
-- [...]
--


--
-- constraints
--

select DBMS_METADATA.GET_DDL('CONSTRAINT', constraint_name, owner) as sql
from all_constraints
where owner = 'SYGAL'
      and constraint_type = 'P'
union all
select DBMS_METADATA.GET_DDL('REF_CONSTRAINT', constraint_name, owner) as sql
from all_constraints
where owner = 'SYGAL'
      and constraint_type = 'R'
