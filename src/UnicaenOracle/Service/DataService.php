<?php

namespace UnicaenOracle\Service;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use UnicaenApp\Exception\RuntimeException;

class DataService
{

    /**
     * @param Connection $srcSchemaConn
     * @param string     $schemaName
     * @param string[]   $tableNames
     * @param string     $outputFilePathTemplate
     * @return string[]
     */
    public function createDataInsertsScriptFile(Connection $srcSchemaConn, $schemaName, array $tableNames, $outputFilePathTemplate)
    {
        try {
            $stmt = $srcSchemaConn->prepare($this->generateSQLForTablesColumnsInfos($tableNames));
            $stmt->execute();
        } catch (DBALException $e) {
            throw new RuntimeException("Erreur!", null, $e);
        }
        $results = $stmt->fetchAll();
        $preparedTablesMetadata = $this->prepareTablesMetadata($results);

        $outputFilePaths = [];

        foreach ($preparedTablesMetadata as $tableName => $columnsConfig) {
            $outputFilePath = sprintf($outputFilePathTemplate, $tableName);
            $fh = fopen($outputFilePath, 'w');
            if ($fh === false) {
                throw new RuntimeException("Impossible d'ouvrir en écriture le fichier '$outputFilePath'.");
            }
            fwrite($fh, "set define off ;" . PHP_EOL . PHP_EOL);
            $sqlSelect = "SELECT *  FROM $tableName";
            try {
                $stmt = $srcSchemaConn->prepare($sqlSelect);
                $stmt->execute();
            } catch (DBALException $e) {
                throw new RuntimeException("Erreur!", null, $e);
            }
            while ($row = $stmt->fetch()) {
                $tableMetadata = $preparedTablesMetadata[$tableName];
                $formattedColumnValues = [];
                foreach ($tableMetadata as $columnName => $columnMetadata) {
                    $columnValue = $row[$columnName];
                    if ($columnValue === null) {
                        $formattedColumnValue = 'NULL';
                    } else {
                        $formatters = $columnMetadata['formatters'];
                        $formattedColumnValue = $columnValue;
                        foreach ($formatters as $formatter) {
                            $formattedColumnValue = $formatter($formattedColumnValue);
                        }
                    }
                    $formattedColumnValues[$columnName] = $formattedColumnValue;
                }
                $columnNames = implode(', ', array_keys($formattedColumnValues));
                $columnValues = implode(', ', $formattedColumnValues);
                $sqlInsert = "INSERT INTO $schemaName.$tableName ($columnNames) VALUES ($columnValues) ;";
                fwrite($fh, $sqlInsert . PHP_EOL);
            }
            fclose($fh);
            $outputFilePaths[] = $outputFilePath;

            yield $outputFilePath;
        }
        sort($outputFilePaths);

        return $outputFilePaths;
    }

    /**
     * @param array $tableNames
     * @return string
     */
    function generateSQLForTablesColumnsInfos(array $tableNames)
    {
        // Attention, bricolage pour respecter l'ordre des tables fournies !

        $i = 0;
        $sqlParts = array_map(function($name) use ($i) {
            return sprintf(
                "select %d as table_order, table_name, column_name, column_id, data_type from all_tab_columns where table_name = '%s'",
                ++$i, $name);
        }, $tableNames);

        $unions = implode(
            PHP_EOL . 'union all' . PHP_EOL,
            $sqlParts);

        $template = <<<EOS
select * from (
%s
) 
order by table_order, column_id
EOS;

        return sprintf($template, $unions);
    }

    /**
     * @param array $data
     * @return array
     */
    function prepareTablesMetadata(array $data)
    {
        $formatted = [];
        foreach ($data as $row) {
            $tableName = $row['TABLE_NAME'];
            $colName = $row['COLUMN_NAME'];
            $dataType = $row['DATA_TYPE'];

            $formatters = [];

            //
            // formatteur selon le type de la colonne
            //
            switch (true) {

                case $dataType === 'NUMBER':
                    $formatters[] = function ($value) {
                        return $value;
                    };
                    break;

                case $dataType === 'VARCHAR2':
                    $formatters[] = function ($value) {
                        $value = str_replace("'", "''", $value);

                        return "'$value'";
                    };
                    break;

                case $dataType === 'CLOB':
                    $formatters[] = function ($value) {
                        $value = str_replace("'", "''", $value);
                        // découpage en morceaux de 4000 caractères pour éviter l'erreur ORA-01704: string literal too long.
                        $lineEnding = '######';
                        $lines = explode($lineEnding, rtrim(chunk_split($value, 4000, $lineEnding), $lineEnding));
                        $toClobs = array_map(function ($value) {
                            return "TO_CLOB('$value')";
                        }, $lines);

                        return implode('||', $toClobs);
                    };
                    break;

                case $dataType === 'DATE':
                case substr($dataType, 0, 9) === 'TIMESTAMP':
                    $formatters[] = function ($value) {
                        return "TO_DATE('$value', 'YYYY-MM-DD HH24:MI:SS')";
                    };
                    break;

                default:
                    throw new RuntimeException("Type imprévu rencontré dans la table '$tableName' : $dataType.");
            }

            //
            // formatteur selon le nom de la colonne
            //
            switch (true) {
                // repérage des valeurs de colonnes d'historique, permettant si besoin une substitution de valeur ultérieure
                case $colName === 'HISTO_CREATEUR_ID':
                case $colName === 'HISTO_MODIFICATEUR_ID':
                case $colName === 'HISTO_DESTRUCTEUR_ID':
                    $formatters[] = function ($value) {
                        return '/*HISTO::*/' . $value;
                    };
                    break;
            }

            $cfg = [
                'DATA_TYPE' => $dataType,
                'formatters' => $formatters,
            ];

            if (!isset($formatted[$tableName])) {
                $formatted[$tableName] = [];
            }
            $formatted[$tableName][$colName] = $cfg;
        }

        return $formatted;
    }

}