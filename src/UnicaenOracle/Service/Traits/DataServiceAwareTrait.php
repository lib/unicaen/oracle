<?php

namespace UnicaenOracle\Service\Traits;

use UnicaenOracle\Service\DataService;

trait DataServiceAwareTrait
{
    /**
     * @var DataService
     */
    protected $dataService;

    /**
     * @param DataService $dataService
     */
    public function setDataService(DataService $dataService)
    {
        $this->dataService = $dataService;
    }
}