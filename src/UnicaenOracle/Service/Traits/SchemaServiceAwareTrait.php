<?php

namespace UnicaenOracle\Service\Traits;

use UnicaenOracle\Service\SchemaService;

trait SchemaServiceAwareTrait
{
    /**
     * @var SchemaService
     */
    protected $schemaService;

    /**
     * @param SchemaService $schemaService
     */
    public function setSchemaService(SchemaService $schemaService)
    {
        $this->schemaService = $schemaService;
    }
}