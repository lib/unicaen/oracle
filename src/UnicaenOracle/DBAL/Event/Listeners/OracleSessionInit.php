<?php

namespace UnicaenOracle\DBAL\Event\Listeners;

use Doctrine\DBAL\Event\ConnectionEventArgs;

class OracleSessionInit extends \Doctrine\DBAL\Event\Listeners\OracleSessionInit
{
    /**
     * @param ConnectionEventArgs $args
     *
     * @return void
     * @throws \Doctrine\DBAL\DBALException
     */
    public function postConnect(ConnectionEventArgs $args)
    {
        $params = $args->getConnection()->getParams();

        if (isset($params['CURRENT_SCHEMA'])) {
            $this->_defaultSessionVars['CURRENT_SCHEMA'] = $params['CURRENT_SCHEMA'];
        }

        return parent::postConnect($args);
    }
}