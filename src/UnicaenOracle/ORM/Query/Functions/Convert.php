<?php

namespace UnicaenOracle\ORM\Query\Functions;

use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class Convert extends FunctionNode
{
    public $field;
    public $convertMode;

    public function getSql(SqlWalker $sqlWalker)
    {
        return sprintf('CONVERT(%s, %s)', 
                $this->field->dispatch($sqlWalker), 
                $this->convertMode->dispatch($sqlWalker));
    }
    
    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->field = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->convertMode = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}