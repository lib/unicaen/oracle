<?php

namespace UnicaenOracle\ORM\Query\Functions;

use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Class RegexpCount
 * @see https://stackoverflow.com/questions/32052563/doctrine-and-oracle-regexp-like
 */
class RegexpCount extends FunctionNode
{
    public $field;
    public $posixPattern;

    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->field = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->posixPattern = $parser->StringPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(SqlWalker $sqlWalker)
    {
        return sprintf('REGEXP_COUNT(%s, %s)',
                $this->field->dispatch($sqlWalker), 
                $this->posixPattern->dispatch($sqlWalker));
    }
}