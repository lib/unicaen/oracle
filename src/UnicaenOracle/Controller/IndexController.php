<?php

namespace UnicaenOracle\Controller;

use Doctrine\DBAL\Connection;
use Interop\Container\ContainerInterface;
use UnicaenOracle\Service\Traits\DataServiceAwareTrait;
use UnicaenOracle\Service\Traits\SchemaServiceAwareTrait;
use Zend\Log\LoggerAwareTrait;
use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{
    use SchemaServiceAwareTrait;
    use DataServiceAwareTrait;
    use LoggerAwareTrait;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Action en mode CLI.
     */
    public function generateScriptForSchemaClearingConsoleAction()
    {
        $destDir = $this->params('output-dir', '/tmp'); // ex: '/tmp'
        $connName = $this->params('connection', 'doctrine.connection.orm_default');

        /** @var Connection $connection */
        $connection = $this->container->get($connName);
        $schemaName = $this->schemaService->extractSchemaNameFromConnection($connection);

        $this->log("=====================================");
        $this->log(" Db schema clearing script generator");
        $this->log("=====================================");

        $this->log("# Generating database clearing scripts from schema $schemaName...");
        $outputFilePath = $destDir . "/oracle-clear-schema-$schemaName.sql";
        $this->schemaService->createSchemaClearingScriptFile($connection, $outputFilePath);
        $this->log($outputFilePath);

        $this->log("Done.");

        exit(0);
    }

    /**
     * Action en mode CLI.
     */
    public function generateScriptForSchemaCreationConsoleAction()
    {
        $destDir = $this->params('output-dir', '/tmp'); // ex: '/tmp'
        $srcConnName = $this->params('src-connection', 'doctrine.connection.orm_default');
        $dstConnName = $this->params('dst-connection', 'doctrine.connection.orm_default');
        $refConstraintsIncluded = (bool) $this->params('ref-constraints-included', 1);

        /** @var Connection $srcConn */
        /** @var Connection $dstConn */
        $srcConn = $this->container->get($srcConnName);
        $dstConn = $this->container->get($dstConnName);

        $srcSchemaName = $this->schemaService->extractSchemaNameFromConnection($srcConn);
        $dstSchemaName = $this->schemaService->extractSchemaNameFromConnection($dstConn);

        $this->log("=====================================");
        $this->log(" Db schema creation script generator");
        $this->log("=====================================");

        $this->log("# Generating schema creation script " . ($refConstraintsIncluded ? 'with' : 'without') . " ref constraints from schema $srcSchemaName...");
        $outputFilePath = $destDir . "/oracle-generate-schema-$dstSchemaName-from-$srcSchemaName.sql";
        $this->schemaService->createSchemaCreationScriptFile($srcConn, $dstConn, $refConstraintsIncluded, $outputFilePath);
        $this->log($outputFilePath);

        $this->log("Done.");

        exit(0);
    }

    /**
     * Action en mode CLI.
     */
    public function generateScriptsForDataInsertsConsoleAction()
    {
        $destDir = $this->params('output-dir', '/tmp'); // ex: '/tmp'
        $tableNames = $this->params('tables');
        $srcConnName = $this->params('src-connection', 'doctrine.connection.orm_default');
        $dstConnName = $this->params('dst-connection', 'doctrine.connection.orm_default');

        /** @var Connection $srcConn */
        /** @var Connection $dstConn */
        $srcConn = $this->container->get($srcConnName);
        $dstConn = $this->container->get($dstConnName);

        $srcSchemaName = $this->schemaService->extractSchemaNameFromConnection($srcConn);
        $dstSchemaName = $this->schemaService->extractSchemaNameFromConnection($dstConn);

        $this->log("===================================");
        $this->log(" Db data inserts scripts generator");
        $this->log("===================================");

        $this->log("# Generating data inserts scripts from schema $srcSchemaName...");
        $tableNames = array_filter(array_map('trim', explode(',', $tableNames)));
        $outputFilePathTemplate = $destDir . "/oracle-data-insert-from-$srcSchemaName.%s-into-$dstSchemaName.sql";
        $outputFilePaths = $this->dataService->createDataInsertsScriptFile($srcConn, $dstSchemaName, $tableNames, $outputFilePathTemplate);
        foreach ($outputFilePaths as $outputFilePath) {
            $this->log($outputFilePath);
        }

        $this->log("Done.");

        exit(0);
    }

    /**
     * Action en mode CLI.
     */
    public function generateScriptForRefConstraintsCreationConsoleAction()
    {
        $destDir = $this->params('output-dir', '/tmp'); // ex: '/tmp'
        $srcConnName = $this->params('src-connection', 'doctrine.connection.orm_default');
        $dstConnName = $this->params('dst-connection', 'doctrine.connection.orm_default');

        /** @var Connection $srcConn */
        /** @var Connection $dstConn */
        $srcConn = $this->container->get($srcConnName);
        $dstConn = $this->container->get($dstConnName);

        $srcSchemaName = $this->schemaService->extractSchemaNameFromConnection($srcConn);
        $dstSchemaName = $this->schemaService->extractSchemaNameFromConnection($dstConn);

        $this->log("==============================================");
        $this->log(" Db ref constraints creation script generator");
        $this->log("==============================================");

        $this->log("# Generating ref constraints creation script from schema $srcSchemaName...");
        $outputFilePath = $destDir . "/oracle-generate-ref-constraints-$dstSchemaName-from-$srcSchemaName.sql";
        $this->schemaService->createRefConstraintsCreationScriptFile($srcConn, $dstConn, $outputFilePath);
        $this->log($outputFilePath);

        $this->log("Done.");

        exit(0);
    }

    /**
     * @param string $message
     */
    private function log($message)
    {
        if ($this->logger !== null) {
            $this->logger->info($message);
        } else {
            echo $message . PHP_EOL;
        }
    }
}