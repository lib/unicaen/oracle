<?php

namespace UnicaenOracle\Controller\Factory;

use Interop\Container\ContainerInterface;
use UnicaenOracle\Controller\IndexController;
use UnicaenOracle\Service\DataService;
use UnicaenOracle\Service\SchemaService;

/**
 *
 *
 * @author Unicaen
 */
class IndexControllerFactory
{
    /**
     * @param ContainerInterface $container
     * @return IndexController
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var SchemaService $schemaService */
        $schemaService = $container->get(SchemaService::class);

        /** @var DataService $dataService */
        $dataService = $container->get(DataService::class);

        $controller = new IndexController();
        $controller->setSchemaService($schemaService);
        $controller->setDataService($dataService);
        $controller->setContainer($container);

        return $controller;
    }
}