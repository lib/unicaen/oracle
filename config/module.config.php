<?php

namespace UnicaenOracle;

use Doctrine\DBAL\Driver\OCI8\Driver as OCI8;
use UnicaenOracle\Controller\Factory\IndexControllerFactory;
use UnicaenOracle\Controller\IndexController;
use UnicaenOracle\DBAL\Event\Listeners\OracleSessionInit;
use UnicaenOracle\ORM\Query\Functions\Chr;
use UnicaenOracle\ORM\Query\Functions\CompriseEntre;
use UnicaenOracle\ORM\Query\Functions\Convert;
use UnicaenOracle\ORM\Query\Functions\PasHistorise;
use UnicaenOracle\ORM\Query\Functions\RegexpCount;
use UnicaenOracle\ORM\Query\Functions\Replace;
use UnicaenOracle\Service\DataService;
use UnicaenOracle\Service\SchemaService;

return [
    'doctrine' => [
        'connection'    => [
            'orm_default' => [
                'driver_class' => OCI8::class,
            ],
        ],
        'configuration' => [
            'orm_default' => [
                'string_functions' => [
                    'CHR'     => Chr::class,
                    'CONVERT' => Convert::class,
                    'REPLACE' => Replace::class,
                    'REGEXP_COUNT' => RegexpCount::class,

                    // fonctions du package PL/SQL "UNICAEN_ORACLE" (cf. "../data/package.sql")
                    'compriseEntre' => CompriseEntre::class,

                    // fonction réalisant compriseEntre(histoCreation, histoDestruction, dateObservation)
                    'pasHistorise' => PasHistorise::class,
                ],
            ],
        ],
        'eventmanager'  => [
            'orm_default' => [
                'subscribers' => [
                    OracleSessionInit::class,
                ],
            ],
        ],
    ],
    'console' => [
        'router' => [
            'routes' => [
                'generateScriptForSchemaClearingConsole' => [
                    'type' => 'Simple',
                    'options' => [
                        'route'    => 'generate-script-for-schema-clearing --connection= [--output-dir=]',
                        'defaults' => [
                            'controller' => IndexController::class,
                            'action'     => 'generateScriptForSchemaClearingConsole',
                        ],
                    ],
                ],
                'generateScriptForSchemaCreationConsole' => [
                    'type' => 'Simple',
                    'options' => [
                        'route'    => 'generate-script-for-schema-creation --src-connection= --dst-connection= [--ref-constraints-included=] [--output-dir=]',
                        'defaults' => [
                            'controller' => IndexController::class,
                            'action'     => 'generateScriptForSchemaCreationConsole',
                        ],
                    ],
                ],
                'generateScriptForRefConstraintsCreationConsole' => [
                    'type' => 'Simple',
                    'options' => [
                        'route'    => 'generate-script-for-ref-constraints-creation --src-connection= --dst-connection= [--output-dir=]',
                        'defaults' => [
                            'controller' => IndexController::class,
                            'action'     => 'generateScriptForRefConstraintsCreationConsole',
                        ],
                    ],
                ],
                'generateScriptsForDataInsertsConsole' => [
                    'type' => 'Simple',
                    'options' => [
                        'route'    => 'generate-scripts-for-data-inserts --src-connection= --dst-connection= --tables= [--output-dir=]',
                        'defaults' => [
                            'controller' => IndexController::class,
                            'action'     => 'generateScriptsForDataInsertsConsole',
                        ],
                    ],
                ],
            ],
        ],
        'view_manager' => [
            'display_not_found_reason' => true,
            'display_exceptions'       => true,
        ]
    ],
    'controllers' => [
        'factories' => [
            IndexController::class => IndexControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'invokables' => [
            SchemaService::class => SchemaService::class,
            DataService::class => DataService::class,
        ]
    ]
];
