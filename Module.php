<?php

namespace UnicaenOracle;

use Unicaen\Console\Adapter\AdapterInterface;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * @param AdapterInterface $console
     * @return array
     */
    public function getConsoleUsage(AdapterInterface $console)
    {
        return [
            // command
            'generate-script-for-schema-clearing --connection= [--output-dir=]' =>
                "Générer les scripts SQL de vidage d'un schéma.",
            // parameters
            ['--connection', "Requis. Nom de la connexion Doctrine concernée, ex: 'doctrine.connection.orm_default'."],
            ['--output-dir', "Facultatif. Chemin du répertoire où seront générés les scripts. Par défaut: '/tmp'."],

            // command
            'generate-script-for-schema-creation --src-connection= --dst-connection= [--ref-constraints-included=] [--output-dir=]' =>
                "Générer les scripts SQL de création d'un schéma destination à l'image d'un schéma source.",
            // parameters
            ['--src-connection', "Requis. Nom de la connexion Doctrine source, ex: 'doctrine.connection.orm_default'."],
            ['--dst-connection', "Requis. Nom de la connexion Doctrine destination, ex: 'doctrine.connection.orm_default'."],
            ['--ref-constraints-included', "Facultatif. Faut-il inclure la génération des contraintes de référence ? Valeur par défaut: 1."],
            ['--output-dir', "Facultatif. Chemin du répertoire où seront générés les scripts. Par défaut: '/tmp'."],

            // command
            'generate-script-for-ref-constraints-creation --src-connection= --dst-connection= [--output-dir=]' =>
                "Générer les scripts SQL de création des contraintes de référence d'un schéma source dans un schéma destination.",
            // parameters
            ['--src-connection', "Requis. Nom de la connexion Doctrine source, ex: 'doctrine.connection.orm_default'."],
            ['--dst-connection', "Requis. Nom de la connexion Doctrine destination, ex: 'doctrine.connection.orm_default'."],
            ['--output-dir', "Facultatif. Chemin du répertoire où seront générés les scripts. Par défaut: '/tmp'."],

            // command
            'generate-scripts-for-data-inserts --src-connection=  --dst-connection= --tables= [--output-dir=]' =>
                "Générer les scripts SQL d'insertion des données d'un schéma source dans un schéma destination.",
            // parameters
            ['--src-connection', "Requis. Nom de la connexion Doctrine source, ex: 'doctrine.connection.orm_default'."],
            ['--dst-connection', "Requis. Nom de la connexion Doctrine destination, ex: 'doctrine.connection.orm_default'."],
            ['--tables', "Requis. Noms des tables concernées séparés par une virgule, ex: 'ACTEUR,ATTESTATION,DIFFUSION'."],
            ['--output-dir', "Facultatif. Chemin du répertoire où seront générés les scripts. Par défaut: '/tmp'."],
        ];
    }
}
