create or replace PACKAGE UNICAEN_ORACLE AS

  FUNCTION implode(i_query VARCHAR2, i_seperator VARCHAR2 DEFAULT ',') RETURN VARCHAR2;

  FUNCTION STR_REDUCE( str CLOB ) RETURN CLOB;

  FUNCTION STR_FIND( haystack CLOB, needle VARCHAR2 ) RETURN NUMERIC;

  FUNCTION LIKED( haystack CLOB, needle CLOB ) RETURN NUMERIC;

  FUNCTION COMPRISE_ENTRE( date_debut DATE, date_fin DATE, date_obs DATE DEFAULT NULL, inclusif NUMERIC DEFAULT 0 ) RETURN NUMERIC;

END UNICAEN_ORACLE;

/

create or replace PACKAGE BODY UNICAEN_ORACLE AS

  FUNCTION implode(i_query VARCHAR2, i_seperator VARCHAR2 DEFAULT ',') RETURN VARCHAR2 AS
    l_return CLOB:='';
    l_temp CLOB;
    TYPE r_cursor is REF CURSOR;
    rc r_cursor;
    BEGIN
      OPEN rc FOR i_query;
      LOOP
        FETCH rc INTO L_TEMP;
        EXIT WHEN RC%NOTFOUND;
        l_return:=l_return||L_TEMP||i_seperator;
      END LOOP;
      RETURN RTRIM(l_return,i_seperator);
    END;

  FUNCTION STR_REDUCE( str CLOB ) RETURN CLOB IS
    BEGIN
      RETURN utl_raw.cast_to_varchar2((nlssort(str, 'nls_sort=binary_ai')));
    END;

  FUNCTION STR_FIND( haystack CLOB, needle VARCHAR2 ) RETURN NUMERIC IS
    BEGIN
      IF STR_REDUCE( haystack ) LIKE STR_REDUCE( '%' || needle || '%' ) THEN RETURN 1; END IF;
      RETURN 0;
    END;

  FUNCTION LIKED( haystack CLOB, needle CLOB ) RETURN NUMERIC IS
    BEGIN
      RETURN CASE WHEN STR_REDUCE(haystack) LIKE STR_REDUCE(needle) THEN 1 ELSE 0 END;
    END;

  FUNCTION COMPRISE_ENTRE( date_debut DATE, date_fin DATE, date_obs DATE DEFAULT NULL, inclusif NUMERIC DEFAULT 0 ) RETURN NUMERIC IS
    d_deb DATE;
    d_fin DATE;
    d_obs DATE;
    res NUMERIC;
    BEGIN
      IF inclusif = 1 THEN
        d_obs := TRUNC( COALESCE( d_obs     , SYSDATE ) );
        d_deb := TRUNC( COALESCE( date_debut, d_obs   ) );
        d_fin := TRUNC( COALESCE( date_fin  , d_obs   ) );
        IF d_obs BETWEEN d_deb AND d_fin THEN
          RETURN 1;
        ELSE
          RETURN 0;
        END IF;
      ELSE
        d_obs := TRUNC( COALESCE( d_obs, SYSDATE ) );
        d_deb := TRUNC( date_debut );
        d_fin := TRUNC( date_fin   );

        IF d_deb IS NOT NULL AND NOT d_deb <= d_obs THEN
          RETURN 0;
        END IF;
        IF d_fin IS NOT NULL AND NOT d_obs < d_fin THEN
          RETURN 0;
        END IF;
        RETURN 1;
      END IF;
    END;

END UNICAEN_ORACLE;

/
